#!/usr/bin/env python

import regex as re
import sys

def main(searchText):

    # patterns to match numbered ordinals with suffixes
    PATTERN_ORDINALS_SUFFIX = '[1-9]*(1st|2nd|3rd|[4-9]th)'

    # patterns used to match ordinals in english
    PATTERN_ORDINALS_ONES   = 'first|second|third|fourth|fifth|sixth|seventh|eight|ninth'
    PATTERN_ORDINALS_UNIQ   = 'tenth|eleventh|twelth|thirteenth|fourteenth|fifteenth|sixteenth|seventeenth|eighteenth|nineteenth'
    PATTERN_ORDINALS_TENS   = 'twentieth|thirtieth|fourtieth|fiftieth|sixtieth|seventieth|eightieth|ninetieth'

    # patterns used to match cardinals in english
    PATTERN_CARDINALS_ONES  = 'one|two|three|four|five|six|seven|eight|nine'
    PATTERN_CARDINALS_UNIQ  = 'ten|eleven|twelve|thirteen|fourteen|fifteen|sixteen|seventeen|eighteen|nineteen'
    PATTERN_CARDINALS_TENS  = 'twenty|thirty|fourty|fifty|sixty|seventy|eight|ninety'

    PATTERN_CARDINALS       = '(%s)|(%s)|(%s)' % (PATTERN_CARDINALS_UNIQ , PATTERN_CARDINALS_TENS , PATTERN_CARDINALS_ONES)
    PATTERN_ORDINALS        = '(%s)|(%s)|(%s)' % (PATTERN_ORDINALS_UNIQ , PATTERN_ORDINALS_TENS , PATTERN_ORDINALS_ONES)

    # patterns used to match references to date ranges
    PATTERN_DATE_SPECIFIC   = 'today|yesterday|tomorrow'
    PATTERN_DATE_WEEKDAY    = '((monday|tuesday|wednesday|thursday|friday|saturday|sunday)(s){0,1})|(mon|tue|tues|wed|thu|thur|thurs|fri|sat|sun)'
    PATTERN_DATE_MONTHS     = 'january|february|march|april|may|june|july|august|september|august|october|november|december|jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec'
    PATTERN_DATE_SEASONS    = 'summer|spring|winter|autumn'

    PATTERN_DURATION_UNITS  = '(second|minute|day|week|quarter|month|year)(s){0,1}'
    PATTERN_TIME_UNSPECIFIC = 'morning|afternoon|evening|night'
    PATTERN_TIME_SPECIFIC   = 'now|noon|midday|midnight|dawn|dusk|sunset|sunrise'
    PATTERN_SHIFT           = '(last|previous|next|following){1}'

    # patterns that are incomplete
    PATTERN_TIME            = ''
    PATTERN_TIME_PERIOD     = 'am|pm|a\.m\.|p\.m\.|o\'clock'
    PATTERN_DURATION_APPROX = 'couple|few|some|many'

    # patterns used in matching valid date formats e.g. dd-mm-yyyy
    PATTERN_DATE_DELIMS     = '[- /\.]'
    PATTERN_DATE_DD         = '(0[1-9]|[12][0-9]|3[01])'
    PATTERN_DATE_MM         = '(0[1-9]|1[012])'
    PATTERN_DATE_YYYY       = '(\d{4})'

    # patterns to detect prepositions
    PATTERN_PREPOSITION_UNSPECIFIC  = 'in(\sthe){0,1}'
    PATTERN_PREPOSITION_TIME        = 'at'
    PATTERN_PREPOSITION_SPECIFC     = 'on(\sthe){0,1}'

    PATTERN_TEMPORAL_EXPRESSION = """
        (
            # date matching
            (
                # Matching dates in the format yyyy-mm-dd
                ({date_yyyy}{date_delims}{date_mm}{date_delims}{date_dd})
                |
                # Matching dates in the format yyyy-dd-mm
                ({date_yyyy}{date_delims}{date_dd}{date_delims}{date_mm})
                |
                # Matching dates in the format mm-dd-yyyy
                ({date_mm}{date_delims}{date_dd}{date_delims}{date_yyyy})
                |
                # Matching dates in the format dd-mm-yyyy
                ({date_dd}{date_delims}{date_mm}{date_delims}{date_yyyy})
            )
            |
            # last friday, next april , previous week
            ( ({shift})\s(({date_weekday})|({date_months})|({duration_units})) )
            |
            # in summer, in the winter
            ({preposition_unspecific}\s(time_unspecific_seasons))
            |
            ({date_specific})
            |
            ({ordinals})
            |
            ({cardinals})
            |
            ({date_weekday})
            |
            ({date_months})
            |
            ({duration_units})
            |
            ({time_unspecific})
            |
            ({time_unspecific_seasons})
            |
            ({date_yyyy})
        )
    """
    
    PATTERN_TEMPORAL_EXPRESSION = PATTERN_TEMPORAL_EXPRESSION.format (
              date_delims       = PATTERN_DATE_DELIMS
            , date_dd           = PATTERN_DATE_DD
            , date_mm           = PATTERN_DATE_MM
            , date_yyyy         = PATTERN_DATE_YYYY
            , date_specific     = PATTERN_DATE_SPECIFIC
            , date_weekday      = PATTERN_DATE_WEEKDAY
            , date_months       = PATTERN_DATE_MONTHS
            , ordinals          = PATTERN_ORDINALS
            , cardinals         = PATTERN_CARDINALS
            , duration_units    = PATTERN_DURATION_UNITS
            , shift             = PATTERN_SHIFT
            , time_unspecific   = PATTERN_TIME_UNSPECIFIC
            , time_unspecific_seasons    = PATTERN_DATE_SEASONS
            , preposition_unspecific    = PATTERN_PREPOSITION_UNSPECIFIC
        )

    # comiling the regular expression for speed improvements across multiple uses
    prog = re.compile(PATTERN_TEMPORAL_EXPRESSION, re.IGNORECASE | re.MULTILINE | re.UNICODE | re.DOTALL | re.VERBOSE)

    datePhrase = ''
    separator = ''

    for match in prog.finditer(searchText) :
        datePhrase = '%s%s%s' % (datePhrase , separator , match.group())
        separator = ' '

    return datePhrase

if __name__ == "__main__":
    result = main(sys.argv[1])

    if result :
        print result
