# Date Phrase Identifier

*This script was built as part of a challenge and is not thoroughly tested.*

After completing the challenge, I have since discovered Python's NLTK and the categorizing / tagging of words: http://www.nltk.org/book/ch05.html. I will be reading this in more detail to see how it would have altered my approach.

## Challenge

Please build a program to display the date phrases that exist in a piece of text.

You can use any programming language to complete this task.

Please also provide an explanation of your solution design and any other relevant decisions or assumptions
you’ve made.

Examples:

| input                                         | output                |
| -----                                         | ------                |
| “I went to the park yesterday”                | “yesterday”           |
| “On Friday in two weeks, meet me in the park” | “Friday in two weeks” |
| “2015-01-01”                                  | “2015-01-01"          |

## Usage

A simple python script. Download and execute as:

python datephraseidentifier.py "Search text"

Examples
```
python datephraseidentifier.py "I went to the park yesterday"
yesterday
```

## Analysis of the problem

> Please build a program to display the date phrases that exist in a piece of text.

- "display the date phrases"
    - Assumed there could be more than one date phrase in a given piece of text.
- "piece of text"
    - Assumed the text can be of any length and also multiple lines.
    - Assume we are going to only cover the english language.

> I've attached the test - could you please pass this on to him and ask him to send us his solution tomorrow.

Time limit... at the time of receiving the test it was ~5pm, so less than 24 hours to submission.

> You can use any programming language to complete this task

With the time contraint, it was important to stick with something I was comfortable with.

## My approach

Considering the analysis of the problem, the constraints and my own knowledge / experience of this area (or lack thereof), first steps were to:

*1. Start with something simple*

Not having experience in this area, my first instincts were to either use regular expressions to filter the date phrases. Optionally using a map reduce framework, splitting the sentences into tuples of words.

*2. Perform some research into the topic area*

Specific questions that I was looking to answer:

- Is this a known problem? If so what is the problem domain called.
- Are there known and documented approaches, challenges or pitfalls?
- Is there a preferred solution or a library, tool or API that could be leveraged.
- Is there a dataset available that can be leveraged.

Through my research, I soon understood that:

- This challenge is a natural language processing problem that involves temporal expressions, specifcally the recognition of temporal expressions: https://en.wikipedia.org/wiki/Temporal_expressions
- Approaches include building rules based on grammar or using statistical models (machine learning).
- The Stanford Temporal Tagger SUTime, which is part of the Stanford CoreNLP can be used to recognise temporal expressions. It is built in Java and there is a Python wrapper that has been contributed. The online demo looked promising: http://nlp.stanford.edu:8080/sutime/process
- There are a set of rules for encoding/annotating documents known as the TimeML specification, that is covered in the Temporal Expression Recognition and Normalisation (TERN).
- There was data available from TimeMl, from the TempEval corpus. Unfortunately, many links in this resource were broken at the time of writing: http://www.timeml.org/timebank/timebank.html
- There was a lot to temporal expressions, understanding grammar and prepositions.

More relevant readings and resources:

- Stanford Temporal Tagger, SUTime: https://nlp.stanford.edu/software/sutime.html
- Online demo of Stanford Temporal Tagger, SUTime: http://nlp.stanford.edu:8080/sutime/process
- Python wrapper for Stanford CoreNLP's SUTime Java Library dc: https://github.com/FraBle/python-sutime
- Temporal Expression Recognition and Normalisation in Python: https://github.com/cnorthwood/ternip
- Recognising and Interpreting Named Temporal Expressions: http://www.derczynski.com/sheffield/papers/named_timex.pdf
- Parsing time: Learning to interpret time expressions: https://nlp.stanford.edu/pubs/2012-naacl-temporal.pdf
- Guidelines for temporal expression annotation for english for tempeval 2010: http://www.timeml.org/tempeval2/tempeval2-trial/guidelines/timex3guidelines-072009.pdf

## Solution explaination

I decided to build something simple using regular expressions.

It was important to build a few test cases that would identify how the solution was handling the problem. The initially planned iterations are below:

- Build regular expressions to match temporal expressions across the provided text as a whole. Patterns such as weekdays, months, ordinals, cardinals, durations, exact dates etc.
- Split the provided text into words, using regular expressions to classify each individual word as being as part of the temporal expression or not.
- Implement a separate post process to improve the accuracy.
- Evaluate using an existing library such as the Stanford Temporal Tagger: SUTime

**NB: At the completion of the challenge, I discovered NLTK for Python**

In particular the categorizing and tagging of words in Python: http://www.nltk.org/book/ch05.html.

## Conclusions

I thoroughly enjoyed this challenge and learnt a lot about NLP, temporal expressions and named entity recognition.

### Regular expressions and lack of iterative improvement

Not appreciating the extent of the complexity of temporal expressions in terms of grammar was a huge mistake. When the patterns were too general, the results would identify many more false positives. More specific, more false negatives.

Wikipaedia was right. There would be a huge time investment to get grammar based rules working well.

### Existing library: SUTime using the Python wrapper

https://github.com/FraBle/python-sutime

I attempted to leverage this library, but ran into a roadblock that I was unable to workaround in the timeframe.

```
Traceback (most recent call last):
File "datatime.py", line 9, in <module>
sutime = SUTime(jars=jar_files, mark_time_ranges=True)
File "/usr/local/lib/python2.7/dist-packages/sutime/sutime.py", line 57, in __init__
'edu.stanford.nlp.python.SUTimeWrapper')
File "/usr/local/lib/python2.7/dist-packages/jpype/_jclass.py", line 55, in   JClass
raise _RUNTIMEEXCEPTION.PYEXC("Class %s not found" % name)
jpype._jexception.RuntimeExceptionPyRaisable: java.lang.RuntimeException: Class edu.stanford.nlp.python.SUTimeWrapper not found
```
