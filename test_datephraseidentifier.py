#!/usr/bin/env python

import unittest
from datephraseidentifier import main

class DatePhraseIdentifierTestCase(unittest.TestCase):

    def test_challenge_1(self):
        self.assertEqual(main('I went to the park yesterday'), 'yesterday')

    def test_challenge_2(self):
        self.assertEqual(main('On Friday in two weeks, meet me in the park'), 'Friday in two weeks')

    def test_challenge_3(self):
        self.assertEqual(main('2015-01-01'), '2015-01-01')

    def test_on_weekday(self):
        self.assertEqual(main('I went to the park on Friday'), 'Friday')

    def test_last_weekday(self):
        self.assertEqual(main('I went to the park last Friday'), 'last Friday')
        
    def test_last_weekday2(self):
        self.assertEqual(main('I went to the park last Friday the fourteenth'), 'last Friday fourteenth')

    def test_last_weekday3(self):
        self.assertEqual(main('The last time I went to the park was last Friday the fourteenth'), 'last Friday fourteenth')

    def test_date_yyyy_mm_dd(self):
        self.assertEqual(main('1984-02-24'), '1984-02-24')

    def test_date_yyyy_dd_mm(self):
        self.assertEqual(main('1984-24-02'), '1984-24-02')

    def test_date_mm_dd_yyyy(self):
        self.assertEqual(main('02-24-1984'), '02-24-1984')

    def test_date_dd_mm_yyyy(self):
        self.assertEqual(main('24-02-1984'), '24-02-1984')

    def test_last_unspecifc_day(self):
        self.assertEqual(main('I went to the park in the morning'), 'morning')

    def test_last_unspecifc_day_date(self):
        self.assertEqual(main('I went to the park last Friday morning'), 'last Friday morning')

    def test_last_unspecifc_season(self):
        self.assertEqual(main('I went to the park in the summer.'), 'summer')

    def test_last_unspecifc_month(self):
        self.assertEqual(main('I went to the park in June.'), 'June')

    def test_last_unspecifc_year(self):
        self.assertEqual(main('I went to the park in 2017.'), '2017')

    def test_last_unspecifc_month_year(self):
        self.assertEqual(main('I went to the park in August 2015.'), 'August 2015')

    def test_last_unspecifc_month_year_2(self):
        self.assertEqual(main('I went to the park in August of 2015.'), 'August 2015')

    def test_last_false_postcode(self):
        self.assertEqual(main('I went to the park thats in the postcode 2046.'), '')

    def test_last_specifc_date(self):
        self.assertEqual(main('I went to the park on August 21st, 2015.'), 'August 2015')

if __name__ == '__main__':
    unittest.main()
